# Shadowcker

## Introduction

This is Shadow client in docker.

## Requirements

 - Docker (make sure to add your user to **docker** group and disconnect from X11/reboot)
 - Docker Compose 3.7+
 - Compatible GPU with DRI exposed & rights to access it (NVIDIA's GPU are not supported)
 - X11

## Build & Run

### Intel / AMD
```bash
git clone https://gitlab.com/aar642/shadowcker.git
cd shadowcker
make stable # or you can use: make beta or make alpha to change client level
make start
```

## Troubleshooting

### Arch / Manjaro

```bash
pacman -Syu xorg-xhost
xhost +localhost && xhost +local:docker
```
### Black Screen AMD GPU ###

Recents AMD hardware requires updated kernel (5.8+), for example:

#### Ubuntu 18.04 ####

```bash
sudo apt install linux-image-generic-hwe-18.04-edge
```
and reboot

#### Ubuntu 20.04 ####

```bash
sudo apt install linux-image-generic-hwe-20.04-edge
```
and reboot

### Start with update

Update works, but make the launcher crash. Just re-start it.

## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discord.gg/shadowtech)
- [Discord Shadow ENG](https://shdw.me/discord-en)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow Community Projects](https://discord.gg/9HwHnHq)

## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")

## Contributors
**Drixs#6784**
**Le Panzer de Rodin#9477**
**Camille**
**gtx** and countless others !

## Disclaimer

This is a community project, project is not affliated to Blade in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Blade Group](http://www.blade-group.com/).
